function createNewUser() {
  let names;
  let secondName;
  let dateBerthday;
  do {
    names = prompt("enter your name");
  } while (!isNaN(names) || names === undefined);
  do {
    secondName = prompt("enter your second name");
  } while (!isNaN(secondName) || secondName === undefined);
 do{
    dateBerthday = prompt("your date of birthday dd.mm.yyyy").trim();
 }while( !ValidDateFormat(dateBerthday))
  
 function ValidDateFormat(input) {
    let dateRegex = /^(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[0-2])\.\d{4}$/;
    return dateRegex.test(input);
  }

  let newUser = {
    firstName: names,
    lastName: secondName,
    birthday: dateBerthday,
    age: 0,
    yearBirthday: 0,

    getAge() {
      let year = this.birthday.split(".")[2];
      this.yearBirthday = year;

      let newDatee = new Date();
      let yearFromNewDatee = newDatee.getFullYear();
      this.age = yearFromNewDatee - this.yearBirthday;
      return `user ${this.age} years old`
    },
    getLogin() {
      let firstName = this.firstName.trim().toLowerCase().charAt(0);
      let lastName = this.lastName.trim().toLowerCase();
      return `login: ${firstName}${lastName} `;
    },
    getPassword() {
      let firstName = this.firstName.trim().toUpperCase().charAt(0);
      let lastName = this.lastName.trim().toLowerCase();
      return `password: ${firstName}${lastName}${this.yearBirthday}`;
    },
  };
  console.log(newUser, newUser.getAge(), newUser.getLogin(), newUser.getPassword());
}
createNewUser();
